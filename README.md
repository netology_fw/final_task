# Дипломная работа по курсу DevOps
Студент: Чепурненко Николай  
Группа: DevOps-8  


## Создание облачной инфраструктуры

### Предварительная настройка
Подготовил облачную инфраструктуру в ЯО при помощи Terraform:

Для подготовки перед настройкой инфраструкты создал отдельную [terraform-конфигурацию](https://gitlab.com/netology_fw/ft_terraform/-/tree/master/init), с помощью которой создается сервисный аккаунт и бакет.
1. Для начала необходимо установить и настроить [Yandex Cloud CLI](https://cloud.yandex.ru/docs/cli/quickstart)
2. После разместить в домашней директории пользователя файл `.terraformrc`
      ```js
      provider_installation {
        network_mirror {
          url = "https://terraform-mirror.yandexcloud.net/"
          include = ["registry.terraform.io/*/*"]
        }
        direct {
          exclude = ["registry.terraform.io/*/*"]
        }
      }
      ```
3. [Terraform plan](res/tf_plan_init.md) для этапа подготовки.

По окончании `terraform apply` в корне проекта будет лежать файл `sa_terraform_auth.key` с ключами для сервисного аккаунта и `backend.key` с конфигурацией s3 backend.  

### Создание Kubernetes кластера

1. [Конфигурация Terraform](https://gitlab.com/netology_fw/ft_terraform)
2. В качестве terraform backend использую S3 в yandex cloud
      ```sh
      $ terraform init -backend-config=backend.key
      ```
3. Создал workspace stage
      ```sh
      $ terraform workspace list
        default
      * stage
      ```
3. Создал подсети в трех зонах доступности. [Конфигурация VPC](https://gitlab.com/netology_fw/ft_terraform/-/blob/master/vpc.tf)
2. Воспользовался сервисом Yandex Managed Service for Kubernetes.  
[Конфигурация кластера и нод](https://gitlab.com/netology_fw/ft_terraform/-/blob/master/cluster.tf)
3. [Terraform plan](res/tf_plan_cluster.md)      
3. По окончании конфигурации кластера конфиг для доступа появится в `~/.kube/config`
5. После запуска `terraform apply` проверил, что кластер k8s успешно запустился
      ```sh
      $ kubectl get nodes
      NAME                        STATUS   ROLES    AGE     VERSION
      cl12enmbn92ddj3s31b2-ypyv   Ready    <none>   3m37s   v1.21.5
      cl1d68omrq48fueijj0o-amip   Ready    <none>   3m      v1.21.5
      cl1kuqrnaasnhq0c3v41-iwuc   Ready    <none>   3m41s   v1.21.5

      $ kubectl get pods --all-namespaces
      NAMESPACE     NAME                                                  READY   STATUS    RESTARTS   AGE
      kube-system   calico-node-skspw                                     1/1     Running   0          5m19s
      kube-system   calico-node-t7xlk                                     1/1     Running   0          4m39s
      kube-system   calico-node-xbzlw                                     1/1     Running   0          5m16s
      kube-system   calico-typha-6d7bddfb44-pwrg8                         1/1     Running   0          4m5s
      kube-system   calico-typha-horizontal-autoscaler-8495b957fc-xc5sr   1/1     Running   0          8m31s
      kube-system   calico-typha-vertical-autoscaler-6cc57f94f4-n5cw9     1/1     Running   3          8m31s
      kube-system   coredns-5f8dbbff8f-8qpjp                              1/1     Running   0          4m52s
      kube-system   coredns-5f8dbbff8f-8z2tc                              1/1     Running   0          8m29s
      kube-system   ip-masq-agent-9tm45                                   1/1     Running   0          5m19s
      kube-system   ip-masq-agent-ptf9g                                   1/1     Running   0          5m15s
      kube-system   ip-masq-agent-zvx42                                   1/1     Running   0          4m39s
      kube-system   kube-dns-autoscaler-598db8ff9c-sltmh                  1/1     Running   0          8m18s
      kube-system   kube-proxy-b7tb4                                      1/1     Running   0          4m39s
      kube-system   kube-proxy-bh7nb                                      1/1     Running   0          5m15s
      kube-system   kube-proxy-tm2q8                                      1/1     Running   0          5m19s
      kube-system   metrics-server-v0.3.1-6b998b66d6-d582n                2/2     Running   0          4m52s
      kube-system   npd-v0.8.0-f2qvt                                      1/1     Running   0          4m39s
      kube-system   npd-v0.8.0-lj8kq                                      1/1     Running   0          5m19s
      kube-system   npd-v0.8.0-sqsw9                                      1/1     Running   0          5m15s
      kube-system   yc-disk-csi-node-v2-5hgjj                             6/6     Running   0          5m19s
      kube-system   yc-disk-csi-node-v2-bg8zb                             6/6     Running   0          5m16s
      kube-system   yc-disk-csi-node-v2-l56ls                             6/6     Running   0          4m39s
      ``` 
    Содержимое S3 бакета
    ![bucket content](res/bucket.png)
---
### Создание тестового приложения

Подготовил тестовое приложение и Dockerfile для него.  
[Репозиторий](https://gitlab.com/netology_fw/ft_app)  
[Registry](https://gitlab.com/netology_fw/ft_app/container_registry/3114285)

---
### Подготовка cистемы мониторинга и деплой приложения

1. Задеплоил стек Prometheus-Alertmanager-Grafana, [Helm chart](https://gitlab.com/netology_fw/ft_k8s/-/tree/master/kube-prometheus-stack). В values.yml для Grafana установил тип сервиса LoadBalancer и порт 3000.
      ```sh
      $ helm install kube-monitor ./kube-prometheus-stack
      $ kubectl get pods
        NAME                                                     READY   STATUS    RESTARTS   AGE
        alertmanager-kube-monitor-kube-promethe-alertmanager-0   2/2     Running   0          22m
        kube-monitor-grafana-78db6c7555-8rbx6                    3/3     Running   0          8m31s
        kube-monitor-kube-promethe-operator-7ffffd4bb6-75r7r     1/1     Running   0          22m
        kube-monitor-kube-state-metrics-8498896857-zc4kk         1/1     Running   0          22m
        kube-monitor-prometheus-node-exporter-lzqgh              1/1     Running   0          22m
        kube-monitor-prometheus-node-exporter-qz2td              1/1     Running   0          22m
        kube-monitor-prometheus-node-exporter-s49g4              1/1     Running   0          22m
        prometheus-kube-monitor-kube-promethe-prometheus-0       2/2     Running   0          22m
      ``` 
  ![grafana dashboard](res/grafana.png)

2. Создал [helm chart](https://gitlab.com/netology_fw/ft_app/-/tree/master/helm) для своего приложения, задеплоил в кластер
      ```sh
      $ helm install v1.0.0 ./app
      $ kubectl get pods | grep simple
        simple-app-74f8fc76b9-bjnzn                              1/1     Running   0          27m
        simple-app-74f8fc76b9-dspvw                              1/1     Running   0          27m
        simple-app-74f8fc76b9-srphp                              1/1     Running   0          27m
      ```  
3. Используя [helm chart](https://gitlab.com/netology_fw/ft_k8s/-/tree/master/atlantis) задеплоил в кластер atlantis, создал custom [wokflow](https://gitlab.com/netology_fw/ft_terraform/-/blob/master/atlantis.yaml) для отслеживания применения изменений в инфраструктуре.![atlantis](res/atlantis.png)

4. Создал сервисный аккаунт для деплоя приложения. [Манифест](https://gitlab.com/netology_fw/ft_k8s/-/blob/master/sa.yml). Подготовил отдельный kubeconfig для размещения его на gitlab runner.
---
### Установка и настройка CI/CD

1. Для организации CI/CD использовал Gitlab CI/CD, установил и настроил собственный gitlab runner
![gitlab runner](res/gitlab_runner.png)
Конфигурация раннера:
      ```toml
      # cat /etc/gitlab-runner/config.toml
      concurrent = 1
      check_interval = 0

      [session_server]
        session_timeout = 1800

      [[runners]]
        name = "runner01"
        url = "https://gitlab.com/"
        token = "**********************"
        executor = "docker"
        [runners.custom_build_dir]
        [runners.cache]
          [runners.cache.s3]
          [runners.cache.gcs]
          [runners.cache.azure]
        [runners.docker]
          tls_verify = false
          image = "docker:stable"
          privileged = true
          disable_entrypoint_overwrite = false
          oom_kill_disable = false
          disable_cache = false
          volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]
          shm_size = 0

      [[runners]]
        name = "runner02"
        url = "https://gitlab.com/"
        token = "**********************"
        executor = "shell"
        shell = "bash"
        [runners.custom_build_dir]
        [runners.cache]
          [runners.cache.s3]
          [runners.cache.gcs]
          [runners.cache.azure]
      ```
2. Написал [pipeline](https://gitlab.com/netology_fw/ft_app/-/blob/master/.gitlab-ci.yml) для сборки и деплоя приложения:
  - при пуше в мастер без тега происходит сборка образа с тегом latest
  - при пуше в ветку без тега происходит сборка образа с тегом в виде короткого хэша коммита
  - при пуше в мастер с тегом происхоит сборка с указанным тегом и деплой новой версии в кластер

Привожу пример деплоя новой версии приложения в кластер:
1. Обновляю версию приложения и произвожу пуш в репозиторий с тегом новой версии
![Change app version](res/cicd1.png)
      ```sh
      $ git status
      On branch master
      Your branch is ahead of 'origin/master' by 6 commits.
        (use "git push" to publish your local commits)

      Changes not staged for commit:
        (use "git add <file>..." to update what will be committed)
        (use "git restore <file>..." to discard changes in working directory)
              modified:   index.php

      no changes added to commit (use "git add" and/or "git commit -a")
      ✔ ~/dip/ft_app [master ↑·6|✚ 1] 
      $ git add index.php
      ✔ ~/dip/ft_app [master ↑·6|●1] 
      $ git commit -m "Update to 1.1.3"
      [master 63b3a3c] Update to 1.1.3
      1 file changed, 1 insertion(+), 1 deletion(-)
      ✔ ~/dip/ft_app [master ↑·7|✔] 
      $ TAG=1.1.3 && git tag -f $TAG && git push -f origin $TAG
      Enumerating objects: 5, done.
      Counting objects: 100% (5/5), done.
      Delta compression using up to 12 threads
      Compressing objects: 100% (3/3), done.
      Writing objects: 100% (3/3), 292 bytes | 292.00 KiB/s, done.
      Total 3 (delta 2), reused 0 (delta 0)
      To gitlab.com:netology_fw/ft_app.git
      * [new tag]         1.1.3 -> 1.1.3
      ```
2. Перехожу в интерфейс ci/cd и убеждаюсь, что все шаги выполнены успешно
![Succesful execute](res/cicd2.png)
Образ с нужным тегом успешно загружен в registry  
![Succesful build](res/cicd3.png)
Произведен деплой приложения в кластер  
![Succesful deploy](res/cicd4.png)

3. Проверяю версию приложения  
![Open app](res/cicd5.png)


---
---
## Ссылки на все необходимые ресурсы
[Основной репозиторий](https://gitlab.com/netology_fw/final_task)  

[Репозиторий приложения](https://gitlab.com/netology_fw/ft_app)  
[Registry](https://gitlab.com/netology_fw/ft_app/container_registry/3114285)  
[Приложение](http://51.250.91.58/)

[Репозиторий terraform](https://gitlab.com/netology_fw/ft_terraform)  

[Репозиторий k8s](https://gitlab.com/netology_fw/ft_k8s)  

[Grafana](http://62.84.121.126:3000/) 
Логин: admin  
Пароль: netology  

[Atlantis](http://51.250.35.104:30932/)


---
---
Chepurnenko Nikolay  
2022