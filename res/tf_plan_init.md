```js
Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the
following symbols:
  + create

Terraform will perform the following actions:

  # local_file.backendConf will be created
  + resource "local_file" "backendConf" {
      + content              = (sensitive)
      + directory_permission = "0777"
      + file_permission      = "0777"
      + filename             = "../backend.key"
      + id                   = (known after apply)
    }

  # null_resource.createJson will be created
  + resource "null_resource" "createJson" {
      + id = (known after apply)
    }

  # yandex_iam_service_account.sa-terraform will be created
  + resource "yandex_iam_service_account" "sa-terraform" {
      + created_at = (known after apply)
      + folder_id  = "b1gr35v6g4unttk88aig"
      + id         = (known after apply)
      + name       = "terraform"
    }

  # yandex_iam_service_account_static_access_key.sa-terrafom-static-key will be created
  + resource "yandex_iam_service_account_static_access_key" "sa-terrafom-static-key" {
      + access_key           = (known after apply)
      + created_at           = (known after apply)
      + description          = "static access key for object storage"
      + encrypted_secret_key = (known after apply)
      + id                   = (known after apply)
      + key_fingerprint      = (known after apply)
      + secret_key           = (sensitive value)
      + service_account_id   = (known after apply)
    }

  # yandex_resourcemanager_folder_iam_member.sa-editor will be created
  + resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
      + folder_id = "b1gr35v6g4unttk88aig"
      + id        = (known after apply)
      + member    = (known after apply)
      + role      = "editor"
    }

  # yandex_storage_bucket.tf-backend will be created
  + resource "yandex_storage_bucket" "tf-backend" {
      + access_key         = (known after apply)
      + acl                = "private"
      + bucket             = "tf.jkkoaqsqcfpkmswl"
      + bucket_domain_name = (known after apply)
      + force_destroy      = true
      + id                 = (known after apply)
      + secret_key         = (sensitive value)
      + website_domain     = (known after apply)
      + website_endpoint   = (known after apply)

      + versioning {
          + enabled = (known after apply)
        }
    }

Plan: 6 to add, 0 to change, 0 to destroy
```